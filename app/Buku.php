<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';

    protected $fillable = ['judul','edisi','penerbit','tahun','penulis_id','kategori_id'];
}
