<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use DB;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $penulis  = DB::table('penulis')->get();
        $kategori = DB::table('kategori')->get();
        return view('buku.create', compact('penulis','kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul'       => 'required',
            'edisi'       => 'required',
            'penerbit'    => 'required',
            'tahun'       => 'required',
            'penulis_id'  => 'required',
            'kategori_id' => 'required'

        ]);

        $buku = new Buku;

        $buku->judul        = $request->judul;
        $buku->edisi        = $request->edisi;
        $buku->penerbit     = $request->penerbit;
        $buku->tahun        = $request->tahun;
        $buku->penulis_id   = $request->penulis_id;
        $buku->kategori_id  = $request->kategori_id;

        $buku->save();

        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findOrFail($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penulis  = DB::table('penulis')->get();
        $kategori = DB::table('kategori')->get();
        $buku     = Buku::findOrFail($id);
        return view('buku.edit', compact('buku','penulis','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul'       => 'required',
            'edisi'       => 'required',
            'penerbit'    => 'required',
            'tahun'       => 'required',
            'penulis_id'  => 'required',
            'kategori_id' => 'required'

        ]);


        $buku = Buku::find($id);

        $buku->judul        = $request->judul;
        $buku->edisi        = $request->edisi;
        $buku->penerbit     = $request->penerbit;
        $buku->tahun        = $request->tahun;
        $buku->penulis_id   = $request->penulis_id;
        $buku->kategori_id  = $request->kategori_id;

        $buku->save();

        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku->delete();

        return redirect('/buku');
    }
}
