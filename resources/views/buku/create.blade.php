@extends('layout.master')

@section('judul')
Buku
@endsection

@section('judulbox')
Buku Create
@endsection

@section('content')        
    <form action="/buku" method="post">
        @csrf

        <div class="form-group">
            <label class="col-form-label">Judul</label>
            <input type="text" class="form-control" placeholder="Judul buku" name="judul">
            @error('judul')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label class="col-form-label">Edisi</label>
            <input type="text" class="form-control" placeholder="Edisi" name="edisi">
            @error('edisi')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label class="col-form-label">Penerbit</label>
            <input type="text" class="form-control" placeholder="Penerbit" name="penerbit">
            @error('penerbit')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label class="col-form-label">Tahun</label>
            <input type="number" class="form-control" placeholder="Tahun" name="tahun">
            @error('tahun')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label class="col-form-label">Penulis</label>
            <select class="form-control" name="penulis_id">
                <option value="">-- Pilih Penulis --</option>
                @foreach ($penulis as $itemp)
                    <option value="{{ $itemp->id }}">{{ $itemp->nama }}</option>
                @endforeach
            </select>
            @error('penulis_id')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label class="col-form-label">Kategori</label>
            <select class="form-control" name="kategori_id">
                <option value="">-- Pilih Kategori --</option>
                @foreach ($kategori as $itemk)
                    <option value="{{ $itemk->id }}">{{ $itemk->nama }}</option>
                @endforeach
            </select>
            @error('kategori_id')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <input type="submit" value="Submit" class="btn btn-primary">
        <a href="/buku" class="btn btn-danger">Cancel</a>
    </form>
@endsection
