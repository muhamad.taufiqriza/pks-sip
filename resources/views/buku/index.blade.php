@extends('layout.master')

@section('judul')
Data buku
@endsection

@section('judulbox')
Data All buku
@endsection

@section('content')
  <a href="/buku/create" class="btn btn-primary mb-4"><i class="fas fa-plus"></i> Add Data</a>
  <div class="card">
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Judul</th>
            <th>Tahun</th>
            <th>Penerbit</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($buku as $key => $item)
            <tr>
              <td>{{$key + 1}}</td>

              <td>{{$item->judul}}</td>
              <td>{{$item->tahun}}</td>
              <td>{{$item->penerbit}}</td>
              <td>
                <form action="/buku/{{$item->id}}" method="post">
                  <a href="/buku/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
                  <a href="/buku/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  
                  @csrf
                  @method('delete')
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
                {{-- <a href="/buku/create" class="btn btn-danger btn-sm">Delete</a> --}}
              </td>
            </tr>

          @empty
            <tr>
              <td colspan="5">Data Masih Kosong</td>
            </tr>

          @endforelse
        </tbody>
        <tfoot>
          <tr>
            <th>#</th>
            <th>Judul</th>
            <th>Tahun</th>
            <th>Penerbit</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection

@push('script')
  <!-- DataTables -->
  <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush

@push('style')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endpush
